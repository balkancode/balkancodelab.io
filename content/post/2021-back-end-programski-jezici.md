---
authors: [ dusan-d ]
title: "Koji programski jezik izabrati za Back-End? | 2021"
date: 2021-04-25
hero: /images/2021-back-end-languages.jpg
excerpt: "Python, Ruby, PHP, Java, Javascript, Golang, Rust, Elixir... Koji jezik (i ekosistem) je najbolji izbor za back-end programiranje u 2021. godini?"
draft: false
---

Koji programski jezik je najbolji izbor za **back-end programiranje u 2021.
godini?** Ova objava će vam olakšati izbor predstavljajući neke od prednosti i
mana popularnih programskih jezika i njihovih ekosistema.

Video:

{{< youtube NNZoNtchWkw >}}



-------------------------------------------------------------------------------

> A language that doesn't affect the way you think about programming is not worth knowing.

-------------------------------------------------------------------------------

## 1. Python

#### Prednosti:
- Lako se uči
- Postoji dobar izbor web framework-a
- Dobar izbor za eventualni prelazak na AI, Data Science, Machine Learning itd.

#### Mane:
- Spor (generalno)
- Iako je dosta popularan, nije pretjerano popularan za Back-End
- Nema neke pretjerane potražnje na našem tržištu

#### Preporuka: NE

-------------------------------------------------------------------------------

## 2. Ruby

#### Prednosti:
- Sjajan ekosistem
- Jako prijatna sintaksa
- Izuzetno produktivan

#### Mane:
- Spor (generalno)
- Slabo se koristi za bilo šta osim Back-End programiranja
- Opada mu popularnost

#### Preporuka: NE

-------------------------------------------------------------------------------

## 3. PHP

#### Prednosti:
- Sjajan ekosistem
- Zreo community
- PHP8 je izuzetno brz
- Postoji dobar izbor web framework-a
- Wordpress je pisan u PHP-u, što znači da ima dosta posla (i biće)

#### Mane:
- Postoji veliki broj legacy projekata pisanih u PHP-u, i velike su šanse su da
  će da vam zapadne da radite na jednom od njih (što nije samo po sebi loše)
- Na lošem je glasu
- PHP poslovi znaju biti slabije plaćeni (ali ne nužno)

#### Preporuka: DA

-------------------------------------------------------------------------------

## 4. Java

#### Prednosti:
- Ozbiljan ekosistem
- Produktivan jezik za rad

#### Mane:
- Naporna sintaksa
- Zahtjeva alate kako bi efikasno moglo da se radi
- JVM je dosta zahtjevan
- Uglavnom se radi na legacy projektima
- Nije najbolji izbor za modernu infrastrukturu

#### Preporuka: NE

-------------------------------------------------------------------------------

## 5. Javascript

#### Prednosti:
- Vrijedi ga naučiti jer se svakako ne može izbjeći na web-u
- Sintaksa zna biti elegantna
- Dobar izbor za Full-Stack Development

#### Mane:
- Jako neozbiljan ekosistem
- Jezik je dosta loše dizajniran
- Slab izbor framework-a

#### Preporuka: NE

-------------------------------------------------------------------------------

## 6. Golang

#### Prednosti:
- Jezik je odlično dizajniran
- Izuzetno brz
- Odličan izbor za modernu infrastrukturu

#### Mane:
- Djeluje da se Google odaljava od Go-a
- Sintaksa nije toliko deklarativna

#### Preporuka: DA (ali ne za početnike)

-------------------------------------------------------------------------------

## 7. Rust

#### Prednosti:
- Izuzetno brz
- Odličan izbor za modernu infrastrukturu

#### Mane:
- Teško se uči
- Nema potrebe u njemu raditi web (Back-End), nije namjenjen za to

#### Preporuka: NE

-------------------------------------------------------------------------------

## 8. Elixir

#### Prednosti:
- Izuzetno prijatan za rad
- Dosta je produktivan
- Funkcionalna paradigma
- Odličan za real-time programiranje

#### Mane:
- Funkcionalna paradigma (teže se uči i teže se nalazi posao)
- Ekosistem nije baš najrazvijeniji

#### Preporuka: NE (DA, za nekog ko hoće nešto više ili drugačije)
