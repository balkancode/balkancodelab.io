---
authors: [ dusan-d ]
title: "Da li je neophodno znati Algoritme? (kao početnik)"
date: 2021-10-10T12:00:00+02:00
hero: /images/2021_algoritmi.jpg
excerpt: Da li početnici moraju znati algoritme, i da li ih vrijedi učiti?
draft: false
---

Video:

{{< youtube 2AHLUwTCs6s >}}

-------------------------------------------------------------------------------

Ako radite web ili mobilne aplikacije - ja mislim da je gubljene vremena
ulaziti previše duboko u algoritme. 

Skoro nikad nećete pisati svoj algoritam kako bi riješili neki problem na
projektu. Obično se koriste gotova rješenja koja nude biblioteke ili jezik.

Ja sam u svojoj karijeri ručno pisao algoritam možda ukupno 2-3 puta.

Takođe dosta algoritama se zasniva na rekurziji, a rekurziju se skoro nikad
nikada neće pojaviti u kodu kojeg ćete pisati na projektima. Pored toga, svi
osnovni problemi koji se rješavaju rekurzijom se mogu riješiti iteracijom ili
higher-order funkcijama. Iteracija je mnogostruko efikasnija osim u slučajevima
kada koristite neki funkcionalni jezik. [Funkcionalni
jezici](https://en.wikipedia.org/wiki/Functional_programming) su obično
optimizovani za rekruziju, a iteracija je obično sporija jer se oslanja na
mutaciju varijabli, koja je skupa u funkcionalnoj paradigmi.

## Gdje se zapravo pojavljuju algoritmi?

Algoritmi se obično pojavljuju samo na intervjuima, ali uglavnom u kompanijama
kao što su Microsoft, Google, Amazon, Facebook itd.  Takođe "top talent"
sajtovi poput Turinga i TopTala traze dobro poznavanje algoritama, sturktura
podataka i dinamičkog programiranja. Osim toga, ako ste web, ili mobile
developer - to vam sve uglavnom neće trebati.

Algoritamski zadaci mogu da se pojave tu i tamo na intervjuima, ali to ce biti
najjednostavniji mogući - tako da preporučujem da njih savladate ali tu ne
vrijedi pretjerivati... Znači pogledati osnovne strukture podataka, _search_ i
_sorting_ algoritme i to je to.

## Algoritmi su (naravno) korisni

Nije ih loše znati kako bi postali bolji programer, ali nije neophodno.
Naučiće vas da razimšljate "programerski": da razbijate problem u manje
dijelove, da optimizujete rješenja, da razmišljate o tome koliko koja operacija
košta itd. Takođe - vremenom ćete izgraditi bazu gotovih algoritamskih rješenja
za razne probleme. Nije da ce vam ta rješenja trebati na poslu ali eto...

## Isplativije je raditi na konkretnim stvarima

Ulažite vrijeme u učenje vašeg odabranog programskog jezika i njegovih
svojstava, vašeg odabranog frameworka i izgradnju realnih projekata.  Algoritmi
su za početnike prilicno gubljenje vremena, naročito ako je cilj doći do
plaćenog posla ili izgradnja nekog konkretnog projekta.
