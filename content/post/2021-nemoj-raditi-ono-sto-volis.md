---
title: "Nemoj raditi ono što voliš! Radi ono u čemu si dobar (dobra)!"
date: 2021-08-22T22:05:02+02:00
hero: /images/hero-3.jpg
excerpt:
draft: true
---

Tags: savet savjet karijera

Pozdrav svima, ja sam Dušan sa sajta `balkancode.rs` a u ovom videu ćemo malo
da se kritički osvrnemo na izreke "radi ono što voliš, prati svoju strast",
vidjecemo da li je dobro voditi se tim izrekama pri izboru karijere.

Ja cu izneti neka svoja razmišljanja na tu temu a vi nakon gledanja uključite
SVOJ mozak, oformite neko svoje mišljenje i podijelite sa ostalima u
komentarima ako hoćete.

-------------------------------------------------------------------------------

Ovaj video je zasnovan na knjizi _Cal Newport_-a koja se zove **So Good They
Can't Ignore You**. Ta knjiga mi godinama stoji na listi i evo tek sam je skoro
procitao, pa bi neke stvari iz te knjige da podijelim sa vama.

Mozda je i bolje sto sam je tek sad procitao, jer sad imam neki zreliji i
visedimenzionalniji pogled na svijet nego recimo prije 7-8 godina kad sam se
bas lozio na taj self-development i kad sam intenzivno citao te knjige.

-------------------------------------------------------------------------------

"Radi ono što voliš" je američka floskula koja je došla i na naše prostore,
koja nam govori da trebamo izabrati profesiju prema tome šta strastveno volimo
i u čemu uživamo.

---

Identifikovati ljubav prema necemu i oko toga graditi karijeru

# Chapter 1 - The "follow your passion" myth"

Procenat Amerikanaca koji tvrde da vole svoj posao:
1987 - 61%
2010 - 45%

(U.S. Job Satisfaction Survey)

Ako mozemo kontrolisati sta cemo raditi u zivotu, zasto ne bi radili nesto sto
volimo ?

Autor je trazio koliko put se fraza "follow your passion" pojavljuje u knjigama
koristeci Google Ngram i to mu je pokazalo da je ta fraza bila stabilno popularna od
kako je prvi put usla u modu 1970ih, ali da od 1990e ucestalost te fraze raste
i od 2000te se 3 puta vise pojavljuje nego 70ih i 80ih.

Autor tvrdi da se mladi, vodjeni tim principom, izgube u zivotu i da upadnu u
krizu u svojim 20im godinama, jer nisu sigurni da li im posao koji rade zaista
odgovara, da li zaista prate svoju strast. Ili su izabrali posao koji im ne
odgovara zato sto suse navodili tim pogresnim principom. I onda se preispituju
itd.

Autor tvrdi da "raditi ono sto volis" nije bas pametno kada je u pitanju izbor
profesije.

Autor tvrdi da zbog tog pogresnog pristupa ljudi postanu nesrecni i
nezadovoljni svojim poslom.

# Rule #2 - the Craftsman Mindset

Craftsman mindset: Focus on what value you're producing in your job
Passion   mindset: Focus on what value your job offers you

Author: "The craftsman mindset is the foundation for creating work you love"

Autor povezuje vrhunske muzicare sa ti principom "Be so good they can't ignore you"

Ja sam bio u mladosti muzicar, bio sam eksponiran muzici virtuoza i inspirisan njima.
Tezio sam da dostignem taj nivo.

Naravno kao muzicar, moras imati disciplinu i uloziti sate i sate. Kada sam te
navike prenjeo na programiranje, jako lako sam napredovao.



# CI TESTING
- Test out `dev01/ci-testing` pipeline
- Test out `develop` pipeline
- Test out `tags` pipeline
- Test out `scheduled` pipeline


-------------------------------------------------------------------------------

1. Mit: "Prati svoju strast" je mit

- dodati onaj newspaper i book research sto je autor radio - kada su se
  pojavile te izreke u knjigama i novinama.

Treba da radiš ono što voliš ? Ili, treba da volis ono sto radis? - pitanje je
sad :)


* Radoznalost > strast

Strast je lozenje bezveze, a radoznalost je jaca i dugorocnija

* Citajte knjige
