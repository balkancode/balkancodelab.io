---
authors: [ dusan-d ]
title: "Da li učiti JavaScript kao svoj prvi programski jezik? ☕"
date: 2021-09-15T12:46:23+02:00
hero: /images/2021_prvo_nauciti_javascript.jpg
excerpt: U ovom tekstu razmatramo da li je pametno izabrati JavaScript kao svoj prvi programski jezik.
draft: false
---

Pozdrav svima, u ovom tekstu razmatramo da li je pametno izabrati
**JavaScript** (skraćeno: JS) kao svoj prvi programski jezik.

TL;DR: Ja mislim da nije! 👎

Video:

{{< youtube 1eghYNYmCqc >}}

## JavaScript danas (2021)

Ako želite postati **programer**, najčešće ćete dobijati savjet da naučite
**JavaScript**, jer je to navodno najbrži način da se dođe do posla 💻.

**JavaScript** je jezik koji je namjenjen je za unošenje dinamike u _Web
Interfejse_, i dizajniran je za _asinhroni_ rad sa `"event"`-ima. On taj posao
radi izuzetno dobro, ali na žalost (kao i neki drugi popularni programski
jezici), razvio se u nešto što nikada nije trebao da bude, sa ciljem da mu se
proširi upotrebljivost i popularnost.

Već duži niz godina se nastoji da **JavaScript** bude korišćen za bukvalno SVE, što
naravno nije tehnički optimalno i poželjno.

Ta sveprisutnost **JavaScripta** naravno odgovara kompanijama koje žele da imaju
standardizovanu tehnologiju i samim tim lako zamenjivu radnu snagu.

Takvom "evolucijom" jezika, došli smo do ovog monstruma kojeg imamo danas,
koji apsolutno nije jednostavan za naučiti i (zaista) shvatiti.

To nas dovodi do mog prvog arugmenta **protiv JavaScripta kao prvog programskog
jezika za početnike**:

## 1. JavaScript je prekomplikovan 🥵

Da, JavaScript je jako komplikovan, i ko ne misli tako vjerovatno nije ulazio u
dubinu jezika i specifikacije, ili jednostavno precjenjuje sebe.

Skoro niko za sebe ne može da tvrdi da zna **JavaScript**. Tu nesigurnost
reflektuju i naslovi knjiga Kyle Simpson-a [You Don't Know
JS](https://github.com/getify/You-Dont-Know-JS). Ja prvi ne mogu da tvrdim da
znam **JavaScript** iako sam uložio dosta godina u učenje i primjenu istog.
Detaljno sam izučio sve relevantne knjige i materijale (po nekoliko puta), ali
**ECMAScript specifikacija** je jednostavno preobimna (često glupa) i zahtjeva
ogroman napor da se zaista savlada.

Početnik bi, po mojoj preporuci, trebalo da krene sa učenjem JavaScripta po **ES5**
standardu, pa onda da na to nadogradi **ES6**, pa **ES2016**, **2017**, **18**, **19**, **20**...

> Početnik bi, po mojoj preporuci, trebalo da krene sa učenjem JavaScripta po
> **ES5** standardu, pa onda da na to nadogradi **ES6**, pa **ES2016**,
> **ES2017**, **ES2018**, **ES2019**, **ES2020**...

Te nadogradnje na **ECMAScript standard** se mogu _relativno lako_ savladati
ali problem je to što je prilično teško savladati i shvatiti sve što obuhvata
**ES5**. Jezik je pun bug-ova i neočekivanog ponašanja, i sve je to potrebno
dobro znati.

Naravno, uvjek se možete zadovoljiti osnovnim razumjevanjem najbitnijih
svojstava jezika i to koristiti da obavljate posao, ali to nikako ne bi
preporučio ljudima kojima bi JavaScript bio **prvi i jedini jezik kojeg će da
koriste**. Konkurencija na **JavaScript** tržištu je prevelika i morate da se
istaknete ako želite da dobijete iole pristojne uslove rada. Ako ćete biti
prosječan JS developer, dosta vam je profitabilinije raditi kao konobar...

> Konkurencija na **JavaScript** tržištu je prevelika i morate da se istaknete
> ako želite da dobijete iole pristojne uslove rada.

Kada na sve to dodamo količinu propratnih alata koje takođe treba savladati, i
koliko često se ti alati mijenjaju - situacija izgleda još gore. (NPM, Yarn,
Webpack, Rollup, Babel, ESLint, Prettier itd. itd. itd...)

## 2. Jezik je pun bug-ova... 👾

Kao što znamo iz priče, **Brendan Eich je navodno razvio JavaScript za 10 dana**,
dok je radio za Netscape 1995. godine. Jezik se je prvobitno nazvan **Mocha** i
nije bio namjenjen za ozbiljno programiranje. Njegova upotreba se proširila
po svim relevantnim brewser-ima, situacija se izmicala kontroli i došlo je do
potrebe da se standardizuje. Tu odgovornost je preuzela ECMA (European Computer
Manufacturers Association) i odatle priča ide dalje.

Potpuno je očekivano da jezik koji je razvijen za 10 dana bude pun bugova i
raznoraznih propusta. Nažalost, toliko se brzo raširio da je postalo kasno
ispraviti sve njegove nedostatke. Developeri koji su ga do tada koristili su se
često oslanjali na te nedostatke, i **ispravke jezika bi prekršile
kompatibilnost i dosta postojećih aplikacija bi prestalo da radi**. Odlučeno je
da jezik ostane kakav jeste i da se jednostavno ide dalje, što je po meni
katastrofalna odluka čije se poslijedice osjete i dan danas.

> Nije iznenađujuće da jezik koji je razvijen za 10 dana bude pun bug-ova i
> raznoraznih propusta.

## 3. Dijelovi nove sintakse su zbunjujući za početnike

Ukoliko odmah krenemo koristiti noviju JavaScript sintaksu (>=**ES6**), bez
razumjevanja prethodnog ponašanja jezika, vjerovatno će nas mnoge stvari
zbunjivati. 

Bez istorijskog konteksta, neki dijelovi sintakse (poput `var`, `let`, i
`const`) su neshvatljivi a nepotrebna sintaksa (poput `class` keyword-a)
prolazi bez kritičkog osvrta.

Ja mislim da je neophodno naučiti prvo **ES5** i njegova ograničenja, da bi
uopšte mogli da shvatimo i kritički pristupimo novijoj sintaksi. Sve to
zahtjeva dosta vremena i nije produktivno za početnike, koji su već dovoljno
zbunjeni i preplavljeni informacijama.

## 4. Ekosistem je izuzetno nezreo (i dalje)

Iako
[statistika](https://2020.stateofjs.com/en-US/demographics/#years_of_experience)
pokazuje da su JavaScript developeri prilično iskusni, u realnosti to meni ne
djeluje tako. Iz mog iskustva JavaScript developeri su uglavnom početnici i
nemaju duboko znanje i iskustvo u softveru. "Zajednica" je izuzetno nezrela i
skoro nikako se ne govori o najboljim praksama, dizajnu softvera i ostalim
bitnim stvarima. Uglavnom se priča vrti oko nekog novog framework-a, novim
svojstvima jezika ili o nekom trenutnom glupom trendu.

To se sve ogleda u kvalitetu **NPM paketa** koji je uglavnom dosta nizak. Čak
su i autori framework-a neiskusni i dosta nepromišljeno pristupaju dizajnu.
Zbog toga imamo milion različitih framework-a, i milion verzija istih.

To stanje ekosistema početnike stavlja u dosta lošu poziciju, gdje uče i
koriste jezik koji im daje previše slobode, a nema jasno uspostavljene prakse i
pravila za dizajniranje kvalitetnih programa. Početnici se nadju u situaciji
gdje imaju previše izbora pri kreiranju rješenja i ne mogu da budu sigurni da
je to što su napravili ispravno i **idiomatski**. Uglavnom biraju da se oslone
na neki _framework_ ili _library_, što ih najčešće još više udaljava od dubokog
razumjevanja samog jezika.

> (stanje ekosistema) početnike stavlja u dosta lošu poziciju, gdje uče i
> koriste jezik koji im daje previše slobode, a nema jasno uspostavljene prakse
> i pravila za dizajniranje kvalitetnih programa.

Sve to loše utiče na razvoj novih programera a naročitno na razvoj njihovog
samopouzdanja.

## 5. Konkurencija na tržištu rada je velika ⚠️

Iz ličnog isksutva znam da firme koje nude početničke JavaScript pozicije ili
prakse dobijaju ogroman broj ponuda, što znači da morate da se prilično jako
istaknete ako želite naći posao, ili mora da imate sreće ili neku vezu.

JavaScript programera početnika ima previše a potražnja za njima je prilično
mala, što te programere naravno stavlja u lošu poziciju. Pošto je ponuda
velika, a potražnja mala, to obično znači i da je kompenzacija
nezadovoljavajuća. Dakle sve i da krenete raditi za neku firmu, plata će vam
vjerovatno biti minimalna. Kao što sam već naveo, dosta vam je profitabilnije
raditi kao konobar...

## 6. Ali želim da radim FrontEnd! 📊

Koga **FrontEnd programiranje** zaista interesuje i ko želi specifično to da
radi, naravno mora da uči i savlada JavaScript. To je potpuno OK, ali bitno je
da što se prije odmaknete od Juniorskog i napredujete do _Intermediate_
nivoa. Jedino će se tako vaš rad finansijski isplatiti.

Ja **svim programerima**, bez obzira na izbor programskog jezika, **preporučujem da
što prije dođu do višeg nivoa znanja i iskustva, i da ciljaju na _Intermediate_
i _Seniorske_ pozicije**.

> Svim programerima, bez obzira na izbor programskog jezika, preporučujem da
> što prije dođu do višeg nivoa znanja i iskustva, i da ciljaju na
> Intermediate i Seniorske pozicije.

To je naravno lako reći ali nije tako lako realizovati. **Neophodno je uložiti
ogromne količine vremena i rada u svoje tehničko usavršavanje, ali i raditi na
ostalim aspektima svoje ličnosti i sposobnosti.** Takođe je izuzetno bitno znati
kuda ići i ne lutati i rasipati energiju bez veze. Zato, **za programere koji
sebe shvataju ozbiljno i koji za sebe žele izuzetne rezultate, nudim plaćeno,
personalizovano mentorstvo, o kome više možete saznati na ovom linku:
[BalkanCode Mentorstvo](/mentor)**.

# 7. Zaključak

To su bili moji argumenti protiv izbora JavaScripta kao prvog programskog
jezika. Naravno, **JavaScript ima i svoje dobre strane, o kojima ćemo pričati
neki naredni put**, a uskoro ću napraviti i kompletan vodič za učenje
JavaScripta od nule. To će da bude odlično, tako da subscribe-ujte se na
[BalkanCode YouTube
kanal](https://www.youtube.com/channel/UC5vvXKI1hkHJ8y3AKMbHSww), podijelite
tekst pa se vidimo u narednoj objavi! 👋