---
authors: [ dusan-d ]
title: "Koliko zarađuju programeri u Srbiji (i ostalim zemljama bivše Jugoslavije)"
date: 2021-08-03T19:22:18+02:00
hero: /images/2021_zarade_programera.jpg
excerpt: Dosta početnika (i ljudi uopšte) se pita - Koliko novca mogu zaraditi kao programer?💰 💶
draft: true
---

Dosta početnika (i ljudi uopšte) se pita: **Koliko novca mogu zaraditi kao
programer 💶 ?**

-------------------------------------------------------------------------------

## Zašto pričati o novcu i zaradi?

**Kao kolege**, zašto da pričamo o novcu i zaradi? Zar to nije neka kao nekulturna,
neprijatna tema koju treba izbjegavati po svaku cijenu itd?

Na to ću vam reći da stanete i malo razmislite o tome ko je uopšte to nametnuo
kao _taboo_ i kome odgovara da se o tome ne priča 🤔?

_HINT: Kompanijama i prevarantima..._

Dakle ja smatram da je finansijska transparentnost jako dobra za radnike i da o
tome treba što više i što otvorenije pričati. Treba pričati i o svim drugim
uslovima rada naravno.

Ja imam jako bitan razlog zašto želim pisati o zaradi - a to je motivisanje
početnika.
 
Svi znamo da se u zemljama bivše Jugoslavije ne živi baš najbolje i da su
finansije većini ljudi uglavnom jako ograničene. Zarade programera su prilično
iznad prosjeka i bitno o tome pričati jer mislim da zarada može i te kako biti
dodatna motivacija i pokretač nekome ko želi da se profesionalno upusti u
programiranje.
 
## Cirfre 💶
 
Dobro, hajde da pređemo na brojeve. Govoriću o bruto zaradi, jer se skoro
svugdje u svijetu govori o bruto iznosima kada se priča o zaradi. 
 
Zarada programera u Srbiji je između **0** i **7-8000€**. To su očigledno dvije
ekstremne cifre i nulu zarađuju ljudi koji su na neplaćenoj praksi, a ovih 7-8
hiljada ljudi koji apsolutno odvaljuju.
 
To su obično ljudi koji rade direktno za klijenta ili preko neke platforme kao
što je TopTal ili slično.
 
Postoje ljudi koji zarađuju i jos više, ali takvih ima baš malo.
 
Ako zanemarimo ove ekstremne slučajeve - najčešća zarada programera se kreće od
**800** do **4000€** - opet da napomenem govorimo o **BRUTO** iznosima.
 
Znači juniori - oko **800€**, seniori do oko **4000**. Neke firme plaćaju vise, neke
manje, ali to su u suštini ti brojevi.
 
Napomenuću da je trenutno u 2021. prosječna bruto plata u Srbiji oko **750e** (~550e
net), znači programerske zarade su uglavnom [iznad državnog
prosjeka](https://www.stat.gov.rs/sr-latn/vesti/statisticalrelease/?p=8292&a=24&s=2403?s=2403).
 
Šta nam govore ovi brojevi? Govore nam da je razlika u zaradama programera jako
velika a ja ću vam iz iskustva reći da vaša zarada u suštini zavisi od vas - od
vašeg tehničkog znanja, iskustva i sposobnosti kao i od nekih ne-tehničkih
stvari poput toga kako se snalazite sa ljudima, kako se snalazite u pregovorima
i tako to. Dakle u suštini od vas zavisi gdje ćete se pozicionirati u ovom
opsegu zarade.

> Vaša zarada u suštini zavisi od vas - od
vašeg tehničkog znanja, iskustva i sposobnosti kao i od nekih ne-tehničkih
stvari poput toga kako se snalazite sa ljudima, kako se snalazite u pregovorima
itd.
 
## Front-End ili Back-End? Gdje je veća zarada?
 
Kada govorimo o “FrontEnd” i “BackEnd” pozicijama, zarade i plate su se tu
otprilike izjednačile jer su obe pozicije dosta zahtjevne i komplikovane kada
pričamo o seniorskom nivou.
 
Za juniore je trenutno malo teže da dođu do dobrog javascript front end posla
jer je konkurencija prilicno velika u tom segmentu, tako da bi početnicima
preporučio da rade back end, ako im je prioritet doći do posla.

> Početnicima bi preporučio da rade Back-End, ako im je prioritet doći do posla.
 
## Nije sve u novcu...
 
Htjeo bi da napomenem jednu jako bitnu stvar - a to je da **nije sve u parama**.
 
Pored zarade potrebno je razmotriti i druge uslove rada i napraviti sebi
odgovarajući balans.
 
Takođe ako vam je zarada glavni motiv za bavljenje programiranjem mislim da
nećete daleko dogurati kao programer. Novac svakako jeste motivacioni faktor
ali daleko od toga da je najefikasniji i najbiniji. 
 
Da bi neko postao vrhunski programer mora imati prirodni afinitet ka tehnici i
ogromnu radoznalost o računarstvu.
 
Eksterni izvori motivacije poput novca nisu kvalitetni i neće vas daleko
dovesti.

Pisaću o motivaciji drugom prilikom.
 
## Zaključak
 
Za kraj da napomenem da sam ovaj članak napisao prvenstveno kako bi nove kolege
mogle da se orijentišu i bolje snađu na tržištu rada.
 
Dakle novac je jako bitan motivacioni faktor ali ako vam je samo novac cilj
programiranje nije dobar izbor, jer nećete istrajati u učenju i razvoju a i
programiranje ne donosi neke velike pare u poređenju sa drugim biznisima.
 
Ako vam je ovo značilo - podijelite ovaj članak i YouTube kanal sa kolegama i
poznanicima, uskoro dolazi jos zanimljivih tema.

Pozdrav!
