---
authors: [ dusan-d ]
title: "Prečice na tastaturi koje SVAKO MORA ZNATI"
date: 2021-06-18
hero: /images/2021_keyboard_shortcuts.png
excerpt: ""
draft: false
---

Video:

{{< youtube rK9yKHQHs8s >}}

## Browser

```
CTRL + L               - Focus na "omnibar"
CTRL + T               - Novi "tab"
CTRL + ENTER           - Dodaje `.com`
CTRL + 1-8             - Skok na "tab" broj 1-8
CTRL + 9               - Skok na zadnji "tab"
CTRL + W               - Gasi "tab" koji je trenutno aktivan
CTRL + TAB             - Naredni "tab"
CTRL + SHIFT + TAB     - Prethodni "tab"
CTRL + SHIFT + T       - Vraća zadnji ugašen "tab"
CTRL + N               - Novi prozor
CTRL + SHIFT + N       - Novi "privatni" prozor
CTRL + L_CLICK         - Otvara link u novom "tabu", fokus ostaje na trenutno aktivnom
CTRL + SHIFT + L_CLICK - Otvara link u novom "tabu", fokus ostaje na trenutno aktivnom
F5 | CTRL + R          - Refresh
CTRL + +/-             - Zoom
ALT + L_ARROW          - Navigacija nazad
ALT + R_ARROW          - Navigacija naprijed
```

## Windows Desktop

```
WIN                - "Brza" pretraga
WIN + D            - "Minimizira" sve "prozore" i baca nas na "Desktop"
WIN + E            - My Computer
WIN + W            - Gasi trenutno aktivni "prozor"
ALT + F4           - Gasi trenutno aktivni "prozor" (pouzdanije ali nezgodno za dohvatiti)
ALT + TAB          - Navigacija kroz otvorene programe - naprijed
ALT + SHIFT + TAB  - Navigacija kroz otvorene programe - nazad
WIN + UP_ARROW     - "Maksimizira" trenutni "prozor"
WIN + DOWN_ARROW   - "Minimizira" trenutni "prozor"
WIN + L            - "Zaključava" ekrana
WIN + X, P         - Control Panel
CTRL + ALT + Del   - Lock Screen
CTRL + SHIFT + Esc - Task Manager
```

## Rad sa fajlovima i direktorijumima (folderima)

```
SHIFT + Delete       - Permanentno brisanje
R_CLICK,      W, T   - Novi teksturalni dokument
CONTEXT_MENU, W, T   - Novi teksturalni dokument (isto kao prethodna komanda, ali bez miša)
F2                   - Preimenovanje fajlova/direktorijuma
ALT + ENTER          - "Properties" trenutno selektovanog fajla/direktorijuma
CTRL + C             - Kopiranje trenutno selektovanog fajla/direktorijuma u "clipboard"
CTRL + X             - Kopiranje trenutno selektovanog fajla/direktorijuma u "clipboard" - za pomjeranje
CTRL + V             - Izbacivanje prethodno kopiranog fajla/direktorijuma iz "clipboard-a" na trenutnu destinaciju
CTRL + SHIFT + N     - Novi direktorijum (folder)
CTRL + A             - Selektovanje svih "ikonica" u trenutnom kontekstu
```

## Rad sa tekstom

```
CTRL + Backspace               - Brisanje cijele "riječi"
CTRL + SHIFT + L_ARROW/R_ARROW - Selektovanje teksta, riječ po riječ
CTRL + Z                       - Poništava prethodnu akciju, i vraća program u prethodno stanje
CTRL + A                       - Selekcija čitavog teksta
```
