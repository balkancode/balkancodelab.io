---
authors: [ dusan-d ]
title: "BRUTALNA MOĆ UNIX TERMINALA: Kako efikasno koristiti bash shell!"
date: 2021-10-08T12:00:00+02:00
hero: /images/2021_terminal.jpg
excerpt:
draft: false
---

Video:

{{< youtube LWtZiK1suXI >}}

-------------------------------------------------------------------------------

## Lista bitnih prečica i komandi (za pojašnjenje pogledaj video):

```
# Shortcuts
CTRL + c <- Cancel the current command
CTRL + r <- Reverse history search
CTRL + l <- Clear the screen (Linux only)
CTRL + a <- Jump to the beginning of the line
ALT  + f <- Jump to the end of the word
CTRL + d <- End of File signal, terminates the shell if the command line is empty

# Aliases                                         # git aliases
alias l='ls -alhF'                                alias gs='git status'
alias ..='cd ..'                                  alias gp='git pull'
alias dev='cd ~/Work/MyProject/'                  alias gb='git branch'
alias pbcopy='xclip -sel clip'                    alias ga='git add .'
alias pbpaste='xclip -selection clipboard -o'     alias gc='git commit -m $1'

# Word Designators:
!$  <- Last word from the preceding command in history
$_  <- Last word from the preceding command, regardless of history
!*  <- All words from the preceding command, except the 0th one (the command name)
!!- <- All words from the preceding command, except the last one

# Event Designators:
!! <- Represents the entire preceeding command

# "-" - meaning depends on the context
cd -           <- Toggles between the current directory, and the previous one
git checkout - <- Toggles between the current git branch, and the previous one

# Directory navigation
pushd - Push directory to   the directory stack
popd  - Pop  directory from the directory stack
dirs  - Show the directory stack
```
