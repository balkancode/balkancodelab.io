---
title: "BalkanCode Mentorstvo 💻"
date: 2021-08-16
# hero: /images/2021-back-end-languages.jpg
draft: false
---

### • Želiš dostići viši nivo u svojoj IT karijeri?
### • Misliš da napreduješ previše sporo?
### • Razmatraš da upišeš kurs ili pohađaš određenu obuku?
### • Pitaš se kako uopšte ući u IT industriju?

---
 
{{< rawhtml >}}
<p style="font-family: var(--serif); font-weight: bold; font-size: 3rem">
Pošalji mail na
<a href="mailto:dusan@balkancode.rs?subject=Mentorski">dusan@balkancode.rs</a>
ukoliko si zainteresovan(a) za
<span style="color: #ff6600">mentorstvo.</span>
</p>
{{< /rawhtml >}}

{{< rawhtml >}}
<h3 style="color: #ff6600">Besplatni mentorski pozivi su zatvoreni.</h3>
{{< /rawhtml >}}

{{< rawhtml >}}

<h2 style="font-size: 4rem; max-width: 840px; margin: 40px 0"> Par utisaka sa mentorskog razgovora:</h2>

<div class="glider-contain" style="margin-bottom: 50px">
  <div class="glider">

    <div class="testimonial">
      <div>
        <img class="testimonial-avatar" src="/images/testimonials/sasa_mrsic.jpeg" />
      </div>
      <div>
        <p style="font-size: 1.4rem">
          <span style="font-family: var(--serif); font-weight: bold; font-size: 2rem">Saša Mrsić</span>
          <br>
          <span class="testimonial-quote">“ </span>
            Moje iskustvo oko poziva je odlično, pre samog poziva sa Dušanom sam veoma lako i brzo dogovorio termin. Dušan je odličan sagovornik koji ima iskustva rada u ovom regionu i osoba koja može da odgovori na vaša pitanja sa pozicije iskusnog programera i da vam da korisne savete za budućnost vaše karjere. Poziv bih ocenio sa 5.
          <span class="testimonial-quote"> ”</span>
        </p>
      </div>
    </div>

    <div class="testimonial">
      <div>
        <img class="testimonial-avatar" src="/images/testimonials/antoni_jozic.jpeg"/>
      </div>
      <div>
        <p style="font-size: 1.4rem">
          <span style="font-family: var(--serif); font-weight: bold; font-size: 2rem">Antoni Jozić:</span>
          <br>
          <span class="testimonial-quote">“ </span>
            Dugo vremena zbog svojih godina (43) sam se lomio oko toga da li krenuti sa programiranjem ili ne.
            Između ostaloga na YouTube-u sam naišao na Dušanov kanal i svidjelo mi se to što je Dusan sve to prezentovao jednim običnim jezikom bez previše kompliciranja.
            Javio sam se Dušanu privatnom porukom i zakazao termin jer jednostavno mi je trebao savjet jednog iskusnog programera.
            Pola sata prošlo kao minuta, što samo po sebi govori da sam uživao u tome razgovoru a ono što je još bitnije je da mi je Dušan objasnio put kojim treba da idem u skladu sa mojim željama i vremenom kojega imam na raspolaganju za učenje.
            Ocjena razgovora čista petica. Hvala Dušane.
          <span class="testimonial-quote"> ”</span>
        </p>
      </div>
    </div>

    <div class="testimonial">
      <div>
        <img class="testimonial-avatar" src="/images/profile-01.jpeg" />
      </div>
      <div>
        <p style="font-size: 1.4rem">
          <span style="font-family: var(--serif); font-weight: bold; font-size: 2rem">David Drobnjak:</span>
          <br>
          <span class="testimonial-quote">“ </span>
            Imao sam lepo iskustvo u mentorskom razgovoru sa Dušanom. Zajedno smo pronašli odgovarajuće smernice za razvoj moje programerske karijere.
          <span class="testimonial-quote"> ”</span>
        </p>
      </div>
    </div>

    <div class="testimonial">
      <div>
        <img class="testimonial-avatar" src="/images/testimonials/mladen_savic.jpeg"/>
      </div>
      <div>
        <p style="font-size: 1.4rem">
          <span style="font-family: var(--serif); font-weight: bold; font-size: 2rem">Mladen Savić</span>
          <br>
          <span class="testimonial-quote">“ </span>
            Imao sam vec ok znanje JS-a kada sam kontaktirao Dusana. Bilo mi je potrebno usmerenje za taj 'naredni korak' i u kratkom i informativnom razgovoru smo prosli kroz relevantne opcije, sto mi je pomoglo da definitivno prelomim i odlucim se za React.
          <span class="testimonial-quote"> ”</span>
        </p>
      </div>
    </div>

    <div class="testimonial">
      <div>
        <img class="testimonial-avatar" src="/images/profile-01.jpeg" />
      </div>
      <div>
        <p style="font-size: 1.4rem">
          <span style="font-family: var(--serif); font-weight: bold; font-size: 2rem">Slobodan Todorović</span>
          <br>
          <span class="testimonial-quote">“ </span>
            Sto se tice poziva i razgovora veoma sam zadovoljan. Veoma instruktivno i zanimljivo i naucio sam jos nekih stvari novih koje se mogu nauciti u 15 min. Preporucio bih svima. Ocena 5+ 🙂.
          <span class="testimonial-quote"> ”</span>
        </p>
      </div>
    </div>

    <div class="testimonial">
      <div>
        <img class="testimonial-avatar" src="/images/testimonials/predrag_stojkovic.jpeg" />
      </div>
      <div>
        <p style="font-size: 1.4rem">
          <span style="font-family: var(--serif); font-weight: bold; font-size: 2rem">
            Predrag Stojković
          </span>
          <br>
          <span class="testimonial-quote">“ </span>
            Razgovor sa Dušanom mi je izuzetno pomogao i dao odgovor na pitanje kako i u kom pravcu treba da se usmerim. Teško je naći pravi put kada postoji toliko tehnologija i alternativa, međutim, Dušan mi je pomogao da otklonim nedoumice.
          <span class="testimonial-quote"> ”</span>
        </p>
      </div>
    </div>

    <div class="testimonial">
      <div>
        <img class="testimonial-avatar" src="/images/testimonials/dusko_kopanja.jpeg" />
      </div>
      <div>
        <p style="font-size: 1.4rem">
          <span style="font-family: var(--serif); font-weight: bold; font-size: 2rem">
            Duško Kopanja
          </span>
          <br>
          <span class="testimonial-quote">“ </span>
            Mentorski sa Dušanom mi je verovatno skraio mesece lutanja i gubljenja vremena. Dobijo sam direktne, konkretne savete i smernice u vezi svih mojih pitanja i nedoumica. Topla preporuka za mentorski i BalkanCode.
          <span class="testimonial-quote"> ”</span>
        </p>
      </div>

    </div>
  </div>

  <button aria-label="Previous" class="glider-prev">‹</button>
  <button aria-label="Next" class="glider-next">›</button>
  <div role="tablist" id="dots"></div>
</div>

{{< /rawhtml >}}

> "Zamisli sledeće: Zarađuješ dobar novac, radiš odakle želiš, radiš posao koji te
> ispunjava i u kome možeš svakodnevno da napreduješ."

Nekima to možda zvuči nerealno, ali mnogi **ljudi u vašem okruženju već žive takav
život**. IT karijera omogućava izuzetno pristojan život na našim prostorima, svi
o tome slušamo svakodnevno. Ali **put do dobrog programerskog posla nije svima tako
jasan i očigledan**. Meni definitivno nije bio.

Kada sam počinjao svoju IT karijeru, **dosta godina sam lutao i učio tehnolgije
koje nisu bile relevantne** na tržištu rada. Nisam imao nikoga ko bi mogao (i
htjeo) da me usmjeri na pravi put. Imao sam odličnu radnu etiku, ali na žalost
dosta truda i energije sam rasipao učeći stvari koje kasnije nikada nisam radio
na projektima. Takođe nisam bio svjestan toga koje su pozicije i tehnologije **NAJPLAĆENIJE**.

Sve u svemu, pravio sam dosta grešaka, ali sam neke stvari takođe radio kako
treba. Svoje dugogodišnje iskustvo u IT industriji dijelim sa ljudima na [YouTube
kanalu](https://www.youtube.com/channel/UC5vvXKI1hkHJ8y3AKMbHSww), a ako ti
treba personalizovan savjet i usmjerenje prijavi se na **BalkanCode mentorski
program**. [Javi se na mail (dusan@balkancode.rs)](mailto:dusan@balkancode.rs)
i zakaži svoj termin za pa da vidimo kako mogu najbolje da te usmjerim i
"poguram".

Želim vam svima mnogo uspjeha u karijeri,

Sve najbolje,

{{< rawhtml >}}
<p>
  <img src="/images/signature.png" alt="Signature" style="width: 50%">
</p>
{{< /rawhtml >}}

{{< rawhtml >}}
<script>

function initializeGlider() {
  new Glider(document.querySelector('.glider'), {
    slidesToShow: 1,
    dots: '#dots',
    draggable: true,
    arrows: {
      prev: '.glider-prev',
      next: '.glider-next'
    }
  });
}

window.addEventListener('load', initializeGlider)
</script>
{{< /rawhtml >}}
